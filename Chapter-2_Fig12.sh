#!/bin/sh

# Load the required data, ends-up in dap.ceda.ac.uk directory and subdirectories

wget -nc -e robots=off --mirror --no-parent -r https://dap.ceda.ac.uk/badc/ar6_wg1/data/ch_02/ch2_fig12/v20220701/

ln -s Chapter-2_Fig12 atomplots

export PYTHONPATH="`pwd`":$PYTHONPATH

cd Chapter-2_Fig12

# First, create and populate the required data directory
mkdir -p data

cd data

# link the directory to the data directory
ln -s ../../dap.ceda.ac.uk/badc/ar6_wg1/data/ch_02/ch2_fig12/v20220701/Figure_2_12a_data_file.nc pressure_mapping.nc

# Create this empty yml configuration file
touch plotconfig.yml

# Link the colormaps in the data directory

ln -s ../../colormaps data/IPCC-WG1-colormaps

cd ..

# Avoid conflict with the package name
mv atmoplots.py main.py

touch __init__.py

# Not working yet, trying to make sense of it
# python main.py --output-dir=.. verticaltrends --var-to-plot=lrt_temperature_altitude --data=("Upper air temperature trends" tdry_trends)

# Restore the directory as it was

mv main.py atmoplots.py
rm -rf data

